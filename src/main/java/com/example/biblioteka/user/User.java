package com.example.biblioteka.user;

import java.time.LocalDate;

public class User {
    private int id;
    private String name;
    private String surname;
    private LocalDate dob;
    private String email;
}
