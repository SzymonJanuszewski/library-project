package com.example.biblioteka.book;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    public List<Book> getBooks() {
        return List.of(
                new Book(
                        1,
                        "Harry Potter i Komnata Tajemnic",
                        "J.K. Rowling",
                        "PL"
                )
        );
    }
}
